<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ViewController extends AbstractController
{
    /**
     * @Route("/view", name="app_view")
     */
    public function index(): Response
    {

        $dia = "Miercoles";
        $usuario = [
            'nombre' => 'Milen',
            'apellido' => 'Ortega',
            'edad' => 23,
            'tipo' => 'admin',

        ];

        $array = array('manzana', 'pero', 'mango');


        return $this->render('view/index.html.twig', [
            'dia_var' => $dia,
            'objeto_usuario' => $usuario,
            'array' => $array
        ]);
    }
}
